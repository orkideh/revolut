### How to run?
 - `yarn`
 - `yarn start`
 
### Notes
 - I have used `useState` and `useReducer` hooks which are familiar to redux as alternative to redux

### features
 - User input filtered
 - rates updated in background each 10 seconds 
 - user can switch currencies
 - check entered amount against balance
 - Typescript
 - decoupled components
 
 
### Rooms for improvement due to short time
 - I considered there will no network issue due to short time, I can add an error handling to let use know if something went wrong.
 - to implement cancelable requests if the previous request has not finished yet 
 - To have a better loading animations
 - Better testing, like E2E and more unit tests
 - To make application prettier
 - to use native number html to provide better user experiance for mobile users 
 
 
 Thank you for review