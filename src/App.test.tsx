import React from 'react';
import { shallow } from 'enzyme';
import 'jest-enzyme';

import App from './App';

it('renders without crashing', () => {
  shallow(<App />);
});

it('renders without crashing', () => {
  const wrapper = shallow(<App />);
  const ThemeProvider = 'ThemeProvider';
  expect(wrapper).toContainMatchingElement(ThemeProvider);
});
