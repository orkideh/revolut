import React from 'react';
import { Container, Grid } from '@material-ui/core';

import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/core/styles';
import theme from './utils/theme';

import Main from './componentes/main';

const symbols = ['USD', 'EUR', 'GBP', 'PLN'];
const initialConvertFrom = 'GBP';
const initialConvertTo = 'EUR';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Container maxWidth="sm">
        <Grid item sm={12}>
          <Main symbols={symbols} initialConvertFrom={initialConvertFrom} initialConvertTo={initialConvertTo} />
        </Grid>
      </Container>
    </ThemeProvider>
  );
}

export default App;
