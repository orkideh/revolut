/**
 * simple helper to generate a new array without selected element
 * @param item
 * @param list
 */
export const exclude = (item: string, list: string[]) => list.filter(i => i !== item);
