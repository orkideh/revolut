import { exclude } from './helper';

it('should remove desired element from array', () => {
  const initialArray = [1, 2, 3, 4, 5];
  const newArray = exclude(4, initialArray);
  const expectedArray = [1, 2, 3, 5];
  expect(newArray.length).toEqual(initialArray.length - 1);
  expect(newArray.join()).toEqual(expectedArray.join());
});
