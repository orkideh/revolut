export const UPDATE_FROM_SYMBOL = 'update from symbol';
export const UPDATE_TO_SYMBOL = 'update to symbol';
export const UPDATE_FROM_AMOUNT = 'update from amount';
export const UPDATE_TO_AMOUNT = 'update to amount';
export const SWITCH_SYMBOLS = 'switch symbols';
