import { IExchangeAction, IExchangeProps } from './exchange.types';
import {
  SWITCH_SYMBOLS,
  UPDATE_FROM_SYMBOL,
  UPDATE_TO_SYMBOL,
  UPDATE_TO_AMOUNT,
  UPDATE_FROM_AMOUNT,
} from './exchange.constants';

/**
 * General action creator to organize actions easier
 * @param dispatch
 * @param rates
 */
export const actionCreator = (dispatch: (action: IExchangeAction) => void, rates: IExchangeProps['initialRates']) => {
  return {
    switchSymbols: () => dispatch({ type: SWITCH_SYMBOLS, payload: { rates } }),
    updateFromSymbol: (symbol: string) => dispatch({ type: UPDATE_FROM_SYMBOL, payload: { symbol, rates } }),
    updateToSymbol: (symbol: string) => dispatch({ type: UPDATE_TO_SYMBOL, payload: { symbol, rates } }),
    updateFromAmount: (amount: string) => dispatch({ type: UPDATE_FROM_AMOUNT, payload: { amount, rates } }),
    updateToAmount: (amount: string) => dispatch({ type: UPDATE_TO_AMOUNT, payload: { amount, rates } }),
  };
};
