import { useEffect, useRef, useState } from 'react';

import { fetchNsetRates } from './exchange.helpers';
import { IExchangeProps } from './exchange.types';

/**
 * custom hook to adapt setInterval with react hooks
 * @param callback
 * @param delay
 */
export function useInterval(callback: any, delay: number) {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick() {
      // @ts-ignore
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

/**
 * custom hook to store latest exchange rates
 * @param symbols
 * @param updateInterval
 * @param initialRates
 */
export const useExchange = (
  symbols: string[],
  updateInterval = 10000,
  initialRates: IExchangeProps['initialRates'],
) => {
  const [rates, setRates] = useState(initialRates);
  useInterval(() => {
    fetchNsetRates(symbols, setRates);
  }, updateInterval);
  return rates;
};
