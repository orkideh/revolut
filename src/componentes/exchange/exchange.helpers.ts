import Axios from 'axios';
import { multiply, number, format } from 'mathjs';

import { IExchangeProps } from './exchange.types';
import { exclude } from '../../helper/helper';

/**
 * util to generate urls automatically
 * @param base
 * @param symbols
 */
export const generateFXUrl = (base: string, symbols: string[]) =>
  `https://api.exchangeratesapi.io/latest?&base=${base}&symbols=${symbols.join(',')}`;

/**
 * fetch a single rate
 * @param base
 * @param symbols
 */
export const fetchFx = async (base: string, symbols: string[]) => {
  const result = await Axios({
    method: 'get',
    url: generateFXUrl(base, symbols),
  });
  return { base, rates: result.data.rates };
};

/**
 * To fetch all the available rate at once
 * @param symbols
 */
export const fetchAllFx = async (symbols: string[]) => {
  const ratesPromise = symbols.map(currency => {
    return fetchFx(currency, exclude(currency, symbols));
  });
  const rates = await Promise.all(ratesPromise);
  return rates.reduce<IExchangeProps['initialRates']>((acc, rate) => {
    acc[rate.base] = rate.rates;
    return acc;
  }, {});
};

/**
 * small util to with a call back to set rates after fetching
 * @param symbols
 * @param cb
 */
export const fetchNsetRates = async (symbols: string[], cb: (rates: IExchangeProps['initialRates']) => void) => {
  const rates = await fetchAllFx(symbols);
  cb(rates);
};

/**
 * General mehtod to update amount for both from and to
 * @param amount
 * @param fromSymbol
 * @param toSymbol
 * @param rates
 */
export const updateAmount = (
  amount: string,
  fromSymbol: string,
  toSymbol: string,
  rates: IExchangeProps['initialRates'],
) => {
  // doing mathematics with mathjs to achive better result than native js
  const result = format(multiply(number(rates[fromSymbol][toSymbol]), number(amount)), {
    notation: 'fixed',
    precision: 2,
  });
  // Trick for a better user experience to avoid showing `0.00`
  if (result === '0.00') {
    return '';
  }
  return format(multiply(number(rates[fromSymbol][toSymbol]), number(amount)), { notation: 'fixed', precision: 2 });
};

/**
 * To check if the amount not larger than the balance
 * @param balance
 * @param amount
 */
export const checkValidBalance = (balance: number, amount: string) => {
  if (amount.length === 0) {
    return false;
  }
  return +amount <= balance;
};
