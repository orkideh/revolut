import { makeStyles } from '@material-ui/core';

export default makeStyles({
  rateWrapper: {
    width: '70%',
    margin: 'auto',
    display: 'flex',
    paddingTop: '20px',
  },
  header: {
    textAlign: 'center',
  },
  submitWrapper: {
    paddingTop: '20px',
    textAlign: 'center',
  },
});
