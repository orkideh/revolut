export interface IRate {
  [name: string]: string;
}
export interface IExchangeProps {
  symbols: string[];
  initialRates: Record<string, IRate>;
  initialConvertFrom: string;
  initialConvertTo: string;
}

export interface IExchangeState {
  toSymbol: string;
  toAmount: string;
  fromSymbol: string;
  fromAmount: string;
  valid: boolean;
  fakeBalance: number;
}

export interface IExchangeAction {
  type: string;
  payload?: any;
}
