import {
  UPDATE_TO_SYMBOL,
  UPDATE_FROM_SYMBOL,
  SWITCH_SYMBOLS,
  UPDATE_TO_AMOUNT,
  UPDATE_FROM_AMOUNT,
} from './exchange.constants';

import { updateAmount, checkValidBalance } from './exchange.helpers';

import { IExchangeState, IExchangeAction } from './exchange.types';

/**
 * Main application reducer
 * @param state
 * @param action
 */
export const exchangeReducer = (state: IExchangeState, action: IExchangeAction) => {
  switch (action.type) {
    case UPDATE_TO_SYMBOL:
      return state.fromSymbol === action.payload.symbol
        ? {
            ...state,
            fromSymbol: state.toSymbol,
            toSymbol: state.fromSymbol,
            toAmount: updateAmount(state.fromAmount, state.toSymbol, state.fromSymbol, action.payload.rates),
            valid: checkValidBalance(state.fakeBalance, state.fromAmount),
          }
        : {
            ...state,
            toSymbol: action.payload.symbol,
            toAmount: updateAmount(state.fromAmount, state.fromSymbol, action.payload.symbol, action.payload.rates),
            valid: checkValidBalance(state.fakeBalance, state.fromAmount),
          };
    case UPDATE_FROM_SYMBOL:
      return state.toSymbol === action.payload.symbol
        ? {
            ...state,
            fromSymbol: state.toSymbol,
            toSymbol: state.fromSymbol,
            toAmount: updateAmount(state.fromAmount, state.toSymbol, state.fromSymbol, action.payload.rates),
            valid: checkValidBalance(
              state.fakeBalance,
              updateAmount(state.fromAmount, state.toSymbol, state.fromSymbol, action.payload.rates),
            ),
          }
        : {
            ...state,
            fromSymbol: action.payload.symbol,
            toAmount: updateAmount(state.fromAmount, action.payload.symbol, state.toSymbol, action.payload.rates),
            valid: checkValidBalance(state.fakeBalance, state.fromAmount),
          };
    case SWITCH_SYMBOLS:
      return {
        ...state,
        fromSymbol: state.toSymbol,
        toSymbol: state.fromSymbol,
        toAmount: updateAmount(state.fromAmount, state.toSymbol, state.fromSymbol, action.payload.rates),
        valid: checkValidBalance(state.fakeBalance, state.fromAmount),
      };
    case UPDATE_TO_AMOUNT:
      return {
        ...state,
        fromAmount: updateAmount(action.payload.amount, state.toSymbol, state.fromSymbol, action.payload.rates),
        toAmount: action.payload.amount,
        valid: checkValidBalance(
          state.fakeBalance,
          updateAmount(action.payload.amount, state.toSymbol, state.fromSymbol, action.payload.rates),
        ),
      };
    case UPDATE_FROM_AMOUNT:
      return {
        ...state,
        toAmount: updateAmount(action.payload.amount, state.fromSymbol, state.toSymbol, action.payload.rates),
        fromAmount: action.payload.amount,
        valid: checkValidBalance(state.fakeBalance, action.payload.amount),
      };
    default:
      throw new Error();
  }
};
