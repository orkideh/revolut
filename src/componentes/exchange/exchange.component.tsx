import React, { useReducer } from 'react';
import { Button } from '@material-ui/core';
import LoopIcon from '@material-ui/icons/Loop';

import ExchangeCard from '../exchange-card';
import { useExchange } from './exchange.hooks';
import { IExchangeProps } from './exchange.types';
import { exchangeReducer } from './exchange.reducer';
import { actionCreator } from './exchange.actions';
import styles from './exchange.styles';

import Rate from '../rate';

/**
 * Main component which has the exchange logic
 * @param symbols
 * @param initialRates
 * @param initialConvertTo
 * @param initialConvertFrom
 * @constructor
 */
const Exchange: React.FC<IExchangeProps> = ({ symbols, initialRates, initialConvertTo, initialConvertFrom }) => {
  const classes = styles();

  const rates = useExchange(symbols, 10000, initialRates);
  const [state, dispatch] = useReducer(exchangeReducer, {
    toSymbol: initialConvertTo,
    fromSymbol: initialConvertFrom,
    fromAmount: '',
    toAmount: '',
    valid: false,
    fakeBalance: 500,
  });

  const actions = actionCreator(dispatch, rates);

  return (
    <div>
      <h3 className={classes.header}>Exchange</h3>
      <ExchangeCard
        activeSymbol={state.fromSymbol}
        symbols={symbols}
        updateSymbol={actions.updateFromSymbol}
        amount={state.fromAmount}
        updateAmount={actions.updateFromAmount}
        balance={state.fakeBalance}
        checkBalance={true}
      />
      <div className={classes.rateWrapper}>
        <Button onClick={actions.switchSymbols}>
          <LoopIcon />
        </Button>
        <Rate base={state.fromSymbol} compareTo={state.toSymbol} rates={rates} />
      </div>
      <ExchangeCard
        activeSymbol={state.toSymbol}
        symbols={symbols}
        updateSymbol={actions.updateToSymbol}
        amount={state.toAmount}
        updateAmount={actions.updateToAmount}
        balance={state.fakeBalance}
      />
      <div className={classes.submitWrapper}>
        <Button variant="contained" color="secondary" disabled={!state.valid}>
          Exchange
        </Button>
      </div>
    </div>
  );
};

export default Exchange;
