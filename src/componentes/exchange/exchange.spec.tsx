import React from 'react';
import renderer from 'react-test-renderer';
import { Input } from '@material-ui/core';
import { Button } from '@material-ui/core';
import LoopIcon from '@material-ui/icons/Loop';

import Exchange from './exchange.component';
import { generateFXUrl, fetchFx, fetchAllFx, fetchNsetRates, checkValidBalance } from './exchange.helpers';

import axios from 'axios';
jest.mock('axios');

describe('Numeric input', () => {
  let ExchangeComponent: renderer.ReactTestRenderer;
  let symbols = ['USD', 'EUR', 'PLN', 'GBP'];
  let initialRates = {
    USD: { EUR: '0.8821453776', PLN: '3.8091037403', GBP: '0.7735532816s' },
    EUR: { PLN: '4.318', USD: '1.1336', GBP: ' 0.8769 ' },
    GBP: { EUR: '1.1403808872', PLN: '4.924164671', USD: '1.2927357737 ' },
    PLN: { EUR: '0.2315886985', USD: '0.2625289486', GBP: '0.2030801297' },
  };
  let initialConvertTo = 'GBP';
  let initialConvertFrom = 'EUR';

  beforeEach(() => {
    ExchangeComponent = renderer.create(
      <Exchange
        symbols={symbols}
        initialRates={initialRates}
        initialConvertTo={initialConvertTo}
        initialConvertFrom={initialConvertFrom}
      />,
    );
  });

  it('renders correctly', () => {
    expect(ExchangeComponent.toJSON()).toMatchSnapshot();
  });

  it('should render input', () => {
    const ExchangeInstance = ExchangeComponent.root;
    expect(ExchangeInstance.findAllByType(Input).length).toEqual(4);
  });

  it('should render Button', () => {
    const ExchangeInstance = ExchangeComponent.root;
    expect(ExchangeInstance.findAllByType(Button).length).toEqual(2);
  });

  it('should render LoopIcon', () => {
    const ExchangeInstance = ExchangeComponent.root;
    expect(ExchangeInstance.findAllByType(LoopIcon).length).toEqual(1);
  });
});

describe('Exchange helpers', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it('generateFXUrl', () => {
    const base = 'USD';
    const symbols = ['PLN', 'EUR'];
    const expectedUrl = 'https://api.exchangeratesapi.io/latest?&base=USD&symbols=PLN,EUR';
    const result = generateFXUrl(base, symbols);
    expect(result).toEqual(expectedUrl);
  });
  it('fetchFx', () => {
    const base = 'USD';
    const symbols = ['PLN', 'EUR'];
    fetchFx(base, symbols);
    expect(axios).toHaveBeenCalledTimes(1);
  });
  it('fetchAllFx', () => {
    const base = 'USD';
    const symbols = ['PLN', 'EUR'];
    fetchAllFx(symbols);
    expect(axios).toHaveBeenCalledTimes(2);
  });
  it('fetchNsetRates', () => {
    const base = 'USD';
    const symbols = ['PLN', 'EUR'];
    fetchNsetRates(symbols, jest.fn);
    expect(axios).toHaveBeenCalledTimes(2);
  });
  it('checkValidBalance check balance larger than amount', () => {
    const balance = 200;
    const amount = '500';
    const res = checkValidBalance(balance, amount);

    expect(res).toEqual(false);
  });
  it('checkValidBalance check balance equal to amount', () => {
    const balance = 200;
    const amount = '200';
    const res = checkValidBalance(balance, amount);

    expect(res).toEqual(true);
  });
  it('checkValidBalance check balance smaller that amount', () => {
    const balance = 200;
    const amount = '100';
    const res = checkValidBalance(balance, amount);

    expect(res).toEqual(true);
  });
  it('checkValidBalance check amount empty', () => {
    const balance = 200;
    const amount = '';
    const res = checkValidBalance(balance, amount);

    expect(res).toEqual(false);
  });
});
