export interface IRate {
  [name: string]: string;
}

export interface IRateProps {
  base: string;
  compareTo: string;
  rates: Record<string, IRate>;
}
