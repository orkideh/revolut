import React from 'react';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import { Typography } from '@material-ui/core';

import { IRateProps } from './rate.types';

/**
 * custom component to demonstrate exchange rate between active currencies
 * @param base
 * @param compareTo
 * @param rates
 * @constructor
 */
const Rate: React.FC<IRateProps> = ({ base, compareTo, rates }) => {
  return (
    <Typography>
      <TrendingUpIcon />1 {base} = {rates[base][compareTo]} {compareTo}
    </Typography>
  );
};

export default Rate;
