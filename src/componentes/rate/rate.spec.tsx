import React from 'react';
import renderer from 'react-test-renderer';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import { Typography } from '@material-ui/core';

import Rate from './rate.component';

describe('Rate', () => {
  let RateComponent: renderer.ReactTestRenderer;
  const rates = {
    EUR: {
      GBR: '1.0002',
    },
  };

  beforeEach(() => {
    RateComponent = renderer.create(<Rate base="EUR" compareTo="GBR" rates={rates} />);
  });

  it('renders correctly', () => {
    expect(RateComponent.toJSON()).toMatchSnapshot();
  });

  it('should render TrendingUpIcon', () => {
    const RateInstance = RateComponent.root;

    expect(RateInstance.findAllByType(TrendingUpIcon).length).toEqual(1);
  });
  it('should render Typography', () => {
    const RateInstance = RateComponent.root;

    expect(RateInstance.findAllByType(Typography).length).toEqual(1);
  });
});
