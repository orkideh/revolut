import React from 'react';
import { FormControl, MenuItem, Select } from '@material-ui/core';

import NumericInput from '../common/numeric-input';

import { IExhangeCardProps } from './exhange-card.types';
import styles from './exchange-card.styles';

/**
 * custom component to contain all the component for send from or send two
 * @param symbols
 * @param activeSymbol
 * @param updateSymbol
 * @param amount
 * @param updateAmount
 * @param balance
 * @param checkBalance
 * @constructor
 */
const ExchangeCard: React.FC<IExhangeCardProps> = ({
  symbols,
  activeSymbol,
  updateSymbol,
  amount,
  updateAmount,
  balance,
  checkBalance = false,
}) => {
  const classes = styles();
  const isBalanceValid = +amount < balance ? '' : classes.red;
  return (
    <div>
      <FormControl className={classes.dropdown}>
        <Select value={activeSymbol} onChange={e => updateSymbol(e.target.value as string)}>
          {symbols.map(item => (
            <MenuItem key={item} value={item}>
              {item}
            </MenuItem>
          ))}
        </Select>
        <span className={checkBalance ? isBalanceValid : ''}> Balance {balance}</span>
      </FormControl>
      <NumericInput amount={amount} onChange={updateAmount} className={classes.input} />
    </div>
  );
};

export default ExchangeCard;
