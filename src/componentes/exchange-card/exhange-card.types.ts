export interface IExhangeCardProps {
  activeSymbol: string;
  symbols: string[];
  updateSymbol: (symbol: string) => void;
  amount: string;
  updateAmount: (amount: string) => void;
  balance: number;
  checkBalance?: boolean;
}
