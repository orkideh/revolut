import { makeStyles } from '@material-ui/core';

export default makeStyles({
  input: {
    width: '80%',
    textAlign: 'right',
  },
  dropdown: {
    width: '20%',
  },
  red: {
    color: 'red',
  },
});
