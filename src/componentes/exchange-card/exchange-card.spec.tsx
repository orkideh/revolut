import React from 'react';
import renderer from 'react-test-renderer';
import { FormControl, Select } from '@material-ui/core';

import ExchangeCard from './exchange-card.component';

describe('ExchangeCard', () => {
  let card: renderer.ReactTestRenderer;
  let symbols = ['PLN', 'USD', 'EUR'];
  let activeSymbol = 'EUR';
  let updateSymbol = jest.fn();
  let amount = '200';
  let updateAmount = jest.fn();
  let balance = 500;

  beforeEach(() => {
    card = renderer.create(
      <ExchangeCard
        symbols={symbols}
        activeSymbol={activeSymbol}
        updateAmount={updateAmount}
        updateSymbol={updateSymbol}
        amount={amount}
        balance={balance}
      />,
    );
  });

  it('renders correctly', () => {
    expect(card.toJSON()).toMatchSnapshot();
  });

  it('should render FormControl', () => {
    const cardInstance = card.root;

    expect(cardInstance.findAllByType(FormControl).length).toEqual(1);
  });

  it('should render Select', () => {
    const cardInstance = card.root;

    expect(cardInstance.findAllByType(Select).length).toEqual(1);
  });
});
