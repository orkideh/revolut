export interface ImainProps {
  symbols: string[];
  initialConvertFrom: string;
  initialConvertTo: string;
}
