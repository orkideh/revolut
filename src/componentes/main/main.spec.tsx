import React from 'react';
import renderer from 'react-test-renderer';

import Main from './main.component';

describe('Numeric input', () => {
  let MainComponent: renderer.ReactTestRenderer;
  let initialConvertFrom = 'GBP';
  let initialConvertTo = 'EUR';
  let symbols = ['EUR', 'GBP', 'USD'];

  beforeEach(() => {
    MainComponent = renderer.create(
      <Main symbols={symbols} initialConvertFrom={initialConvertFrom} initialConvertTo={initialConvertTo} />,
    );
  });

  it('renders correctly', () => {
    expect(MainComponent.toJSON()).toMatchSnapshot();
  });

  it('should render input', () => {
    const MainInstance = MainComponent.root;

    expect(MainInstance.findAllByType('div').length).toEqual(1);
  });

  it('should render loading paragraph', () => {
    const MainInstance = MainComponent.root;

    expect(MainInstance.findAllByType('p').length).toEqual(1);
  });
});
