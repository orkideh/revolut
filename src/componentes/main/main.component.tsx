import React, { useEffect, useState } from 'react';

import Exchange from '../exchange';
import { fetchAllFx } from '../exchange/exchange.helpers';

import { ImainProps } from './main.types';
import styles from './main.styles';

/**
 * A custom component to wrapper Exchange component and initialize it with proper rates
 * @param symbols
 * @param initialConvertFrom
 * @param initialConvertTo
 * @constructor
 */
const Main: React.FC<ImainProps> = ({ symbols, initialConvertFrom, initialConvertTo }) => {
  const classes = styles();
  const [initialRates, setInitialRates] = useState({});

  useEffect(() => {
    fetchAllFx(symbols).then(rates => {
      setInitialRates(rates);
    });
  }, [symbols]);

  return (
    <div className={classes.root}>
      {Object.keys(initialRates).length === 0 && <p>Loading...</p>}
      {Object.keys(initialRates).length > 0 && (
        <Exchange
          symbols={symbols}
          initialRates={initialRates}
          initialConvertTo={initialConvertTo}
          initialConvertFrom={initialConvertFrom}
        />
      )}
    </div>
  );
};

export default Main;
