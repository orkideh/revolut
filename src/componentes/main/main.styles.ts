import { makeStyles } from '@material-ui/core';

export default makeStyles({
  root: {
    boxShadow: '10px 10px 15px -5px rgba(0,0,0,0.75)',
    borderRadius: '10px',
    padding: '15px',
    background: '#fff',
  },
});
