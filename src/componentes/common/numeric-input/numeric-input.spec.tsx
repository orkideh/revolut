import React from 'react';
import renderer from 'react-test-renderer';
import { Input } from '@material-ui/core';

import NumericInput from './numeric-input.component';
import { floatRegExp } from './numeric-input.helpers';

describe('Numeric input', () => {
  let Numeric: renderer.ReactTestRenderer;

  beforeEach(() => {
    Numeric = renderer.create(<NumericInput amount="500" onChange={() => ({})} className="pretty" />);
  });

  it('renders correctly', () => {
    expect(Numeric.toJSON()).toMatchSnapshot();
  });

  it('should render input', () => {
    const NumericInstance = Numeric.root;

    expect(NumericInstance.findAllByType(Input).length).toEqual(1);
  });
});

describe('Valid regex', () => {
  it('should pass against valid formats', () => {
    const valid1 = '12.34';
    expect(floatRegExp.test(valid1)).toEqual(true);
    const valid2 = '1.2';
    expect(floatRegExp.test(valid2)).toEqual(true);
  });

  it('should not pass against invalid formats', () => {
    const invalid1 = 'abs';
    expect(floatRegExp.test(invalid1)).toEqual(false);
    const invalid2 = '12.345';
    expect(floatRegExp.test(invalid2)).toEqual(false);
    const invalid3 = '12.b5';
    expect(floatRegExp.test(invalid3)).toEqual(false);
  });
});
