import React from 'react';
import { Input } from '@material-ui/core';

import { useNumber } from './numeric-input.hooks';
import { Props } from './numeric-input.types';

/**
 * custom controlled input to filter user`s input
 * @param amount
 * @param onChange
 * @param className
 * @constructor
 */
const NumericInput: React.FC<Props> = ({ amount, onChange, className }) => {
  const handleChange = useNumber(onChange);

  return (
    <>
      <Input
        onChange={handleChange}
        value={amount}
        placeholder="0"
        className={className}
        inputProps={{
          style: { textAlign: 'right' },
        }}
      />
    </>
  );
};

export default NumericInput;
