export type Props = {
  amount: string;
  onChange: (value: string) => void;
  className: string;
};
