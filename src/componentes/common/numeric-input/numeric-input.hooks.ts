import { ChangeEventHandler } from 'react';
import { floatRegExp } from './numeric-input.helpers';

/**
 * Custom hook to deliver better user experience and safer input
 * @param onChange
 */
export const useNumber = (onChange: (value: string) => void) => {
  const handleChange: ChangeEventHandler<HTMLInputElement> = e => {
    // test against valid format

    if (e.target.value.replace(/\./g, '').length < 12) {
      if (floatRegExp.test(e.target.value)) {
        onChange(e.target.value.replace(/^0+/, ''));
      }

      // let user to clean input
      if (e.target.value.length === 0) {
        onChange('');
      }
    }
  };
  return handleChange;
};
